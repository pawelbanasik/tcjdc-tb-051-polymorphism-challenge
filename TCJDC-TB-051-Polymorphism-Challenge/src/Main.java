
public class Main {

	public static void main(String[] args) {

		Car car = new Car("Base car", 6);
		car.startEngine();
		car.accelerate();
		car.brake();

		Mitsubishi mitsu1 = new Mitsubishi("Auto Pawla", 4, "red");
		mitsu1.startEngine();
		mitsu1.accelerate();
		mitsu1.brake();

		Ford ford1 = new Ford("Auto sasiada", 4, "niebieski");
		ford1.startEngine();
		ford1.accelerate();
		ford1.brake();

	}
}

class Car {

	private String name;
	private int cylinders;
	private boolean engine;
	private int wheels;

	public Car(String name, int cylinders) {

		this.name = name;
		this.cylinders = cylinders;
		this.engine = true;
		this.wheels = 4;

	}

	public String getName() {
		return name;
	}

	public int getCylinders() {
		return cylinders;
	}

	public void startEngine() {

		System.out.println("Engine of the car started.");

	}

	public void accelerate() {

		System.out.println("Car accelerated.");

	}

	public void brake() {

		System.out.println("Car starts to brake");

	}

}

class Mitsubishi extends Car {

	private String color;

	public Mitsubishi(String name, int cylinders, String color) {
		super(name, cylinders);
		this.color = color;
	}

	@Override
	public void startEngine() {
		System.out.println("Mitsubishi engine starts");

	}

}

class Ford extends Car {

	private String interior;

	public Ford(String name, int cylinders, String interior) {
		super(name, cylinders);
		this.interior = interior;
	}

}
